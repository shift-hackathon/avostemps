# Running locally

## Backend
```shell
> cd back
> python3.11 -m venv venv
> source venv/bin/activate
> flask --app main.py run
````

## Frontend 
```shell
> cd front
> python3.11 -m venv venv
> source venv/bin/activate
> streamlit run main.py
````

##  capture 
```shell
> cd spy
> while true; do echo wait; sleep 5; import -window root  img/screen.png ; sh post.sh ; done
````

